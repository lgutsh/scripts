#!/usr/bin/env zsh
git pull --rebase && git submodule foreach 'git reset --hard && git clean -f -xd' &&  git submodule update --init --recursive
